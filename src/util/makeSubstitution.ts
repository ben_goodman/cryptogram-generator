import BiDirectionalMap from "src/util/BiDirectionalMap"
import { hashString, Cryptogram as ICryptogram } from '../../.aws/terraform/handler/generateCryptogram'


const makeSubstitution = (cryptogram: ICryptogram, substitutionMap: BiDirectionalMap) => {
    const { encoded } = cryptogram
    const decoded = encoded.split('').map(letter => substitutionMap.get(letter) || letter).join('')
    console.log('decoded', decoded)

    const hash = hashString(decoded)
    const isCorrect = hash === cryptogram.plaintextHash

    return {
        decoded,
        isCorrect,
        hash
    }
}

export default makeSubstitution
