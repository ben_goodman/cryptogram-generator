class BiDirectionalMap {
   public reverseMap: Map<string|undefined, string|undefined>;
   public map: Map<string, string|undefined>;

    constructor(iter: Array<[string, string|undefined]>) {
       this.map = new Map(iter);

       this.reverseMap = new Map(
         iter.map( e => e.reverse()) as any
      );
      this.reverseMap.delete(undefined)
    }

   public get(key: string) { return this.map.get(key); }
   public getReverse(key: string) { return this.reverseMap.get(key); }

   public set(key: string, value: string) {
      // use a reverse lookup to find existing assignments
      // e..g, given { a => g, b => f, c => a} and { g => a, f => b, a => c }
      // update b => a, first unset _ => a.  Use reverse-lookup.
      const reverseKey = this.getReverse(value)
      if (reverseKey) {
         this.reverseMap.delete(reverseKey)
         this.map.delete(reverseKey)
      }
      this.map.set(key, value)
      this.reverseMap.set(value, key)
      return this
   }

}

export default BiDirectionalMap
