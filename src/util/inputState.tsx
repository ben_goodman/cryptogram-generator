import { createContext } from "react"
import BiDirectionalMap from "./BiDirectionalMap"

export const DEFAULT_INPUT_STATE = new BiDirectionalMap([
    ['a', ''],
    ['b', ''],
    ['c', ''],
    ['d', ''],
    ['e', ''],
    ['f', ''],
    ['g', ''],
    ['h', ''],
    ['i', ''],
    ['j', ''],
    ['k', ''],
    ['l', ''],
    ['m', ''],
    ['n', ''],
    ['o', ''],
    ['p', ''],
    ['q', ''],
    ['r', ''],
    ['s', ''],
    ['t', ''],
    ['u', ''],
    ['v', ''],
    ['w', ''],
    ['x', ''],
    ['y', ''],
    ['z', ''],
    ['SELECTED', '']
])

export const InputContext = createContext(DEFAULT_INPUT_STATE)