import React, { useState, useEffect } from 'react'
import { type Cryptogram } from '../.aws/terraform/handler/generateCryptogram'
import { deserializeState } from './util/serde'



export interface CryptogramRequest {
    loading: boolean
    error: boolean
    data: Cryptogram | undefined
}

const DEFAULT_REQUEST = {
    loading: true,
    error: false,
    data: undefined,
}


// load cryptogram state from API or from a URL param
const useCryptogram = () => {
    const [cryptogram, setCryptogram] = useState<CryptogramRequest>(DEFAULT_REQUEST)

    useEffect(() => {
        setCryptogram(DEFAULT_REQUEST)

        const [,encodedState] = (
            document.cookie.split(';')
                .map(cookie => cookie.split('=') )
                .filter( cookie => cookie[0] === 'cryptogram' )
        ) [0] || [undefined, undefined]

        const fetchCryptogramAPI = async () => {
            const response = await fetch('https://cryptograms.ben.website/cryptogram/random')
            const data = await response.json()
            setCryptogram({
                loading: false,
                error: false,
                data
            })
        }

        if (encodedState) {
            const state = deserializeState(encodedState)
            setCryptogram(JSON.parse(state))
        } else {
            void fetchCryptogramAPI()
        }
    }, [])

    return cryptogram

}

export default useCryptogram
