import React, { createContext, useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'

import Character, { type CharacterProps } from 'src/components/Character'
import CryptogramAuthor from 'src/components/CryptogramAuthor'
import { Cryptogram as ICryptogram } from '../../.aws/terraform/handler/generateCryptogram'
import { InputContext, DEFAULT_INPUT_STATE } from 'src/util/inputState'
import makeSubstitution from 'src/util/makeSubstitution'

import CheckIcon from 'assets/check.svg'
import SlashIcon from 'assets/slash.svg'

const CryptogramStyleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    margin-bottom: 1rem;
    font-family: "Ubuntu Mono", monospace;
    text-align: center;
`

const WordFragment = styled.div`
    margin-top: 1rem;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`

const generateWordFragments = (data: ICryptogram, setInputState: CharacterProps['setInputState']) => {
    return data.encoded.split(' ').map((word, key) => {

        // creates a word fragment as an Array<Character.Input> for each word.
        const wordArray = word.split('').map((char, key) => {
            // if a special character
            if ( char.match(/[^a-zA-Z]/) ) {
                return <Character.Special key={key} $content={char} />
            }
            const props: CharacterProps = {
                char,
                key,
                setInputState,
            }
            return <Character.Input {...props} key={key} />
        })

        // returns an Array of word fragments.
        return (<WordFragment key={key}>{wordArray}<Character.Whitespace /></WordFragment>)

    })
}

const Preview = styled.p`
    color: #999;
    margin-bottom: 5px;
    margin-top: 3em;
`

const GameStatus = styled.img`
    width: 32px;
    height: 32px;
    margin-left: 1em;
`

const Row = styled.div`
    display: flex;
    flex-direction: row;
    align-items: baseline;
`

const Cryptogram = (data: ICryptogram) => {
    const [inputState, setInputState] = useState(DEFAULT_INPUT_STATE)
    const [subst, setSubst] = useState({
        decoded: 'loading...',
        isCorrect: false,
        hash: 0
    })

    // given the subst. map from the input state and the
    // cryptogram text, solve and hash.
    useEffect(() => {
        setSubst(makeSubstitution(data, inputState))
    }, [inputState])

    useEffect(() => {
        if (subst.isCorrect) {
            document.cookie = 'cryptogram=;max-age=0;path=/'
        }
    } , [subst])

    return (
        <InputContext.Provider value={inputState}>
            <CryptogramStyleWrapper>
                {generateWordFragments(data, setInputState)}
            </CryptogramStyleWrapper>

            <Row>
                <div>
                    <Preview>{subst.decoded}</Preview>
                    <CryptogramAuthor {...data} />
                </div>
                <div>
                    <GameStatus src={subst.isCorrect ? CheckIcon : SlashIcon} />
                </div>
            </Row>

        </InputContext.Provider>
    )
}

export default Cryptogram
