import React from 'react'
import styled from 'styled-components'
import { type Cryptogram as ICryptogram } from '../../.aws/terraform/handler/generateCryptogram'


const StyledCryptogramAuthor = styled.p`
    margin-top: 5px;
    color: #999;
    &:before {
        content: '- ';
    }
`

const CryptogramAuthor = ({author}: ICryptogram) => {
    return (
        <StyledCryptogramAuthor>
            {author}
        </StyledCryptogramAuthor>
    )
}

export default CryptogramAuthor
