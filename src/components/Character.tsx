import React, { useContext, useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import { InputContext } from '../util/inputState'
import BiDirectionalMap from '../util/BiDirectionalMap'


const StyledContainer = styled.div<{ $highlight: boolean }>`
    width: 64px;
    height: 64px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    ${props => props.$highlight && `
        // color: #7ec699;
        color: #cc99cd;
    `}
`

const StyledInput = styled.input.attrs({
    type: "text",
    minLength: 1,
    maxLength: 1,
    autoComplete: "off",
    autoCorrect: "off",
    autoCapitalize: "off",
    spellCheck: "false",
})`
    width: 57px;
    height: 100%;

    text-transform: inherit;
    font-family: inherit;
    font-size: inherit;
    text-align: center;
    font-family: "Ubuntu Mono", monospace;
`


export interface CharacterProps {
    key: React.Key
    char: string
    setInputState?: React.Dispatch<React.SetStateAction<BiDirectionalMap>>
}

const Input = ({
    char,
    setInputState,
}: CharacterProps) => {
    const inputRef = useRef<HTMLInputElement>(null)

    const inputState = useContext(InputContext)


    const handleInput = (e) => {
        const { value } = e.target
        const validation = /^[a-zA-Z]*$/
        if (!validation.test(value)) {
            return
        }
        // check if the value is already in the map
        setInputState?.(currentState => {
            const updateState = currentState.set(char, value)
            const newState = new BiDirectionalMap(Array.from(updateState.map))
            return newState
        })
    }

    const setHighlightedInput = () => {
        inputState.set('SELECTED', '.'+char) //hack to prevent collision with self in reverse map
        const newInputState = new BiDirectionalMap(Array.from(inputState.map))
        setInputState?.(newInputState)
    }

    return (
        <StyledContainer $highlight={inputState.get('SELECTED') === '.'+char}>
            <div>{char}</div>
            <div>
                <StyledInput
                    type="text"
                    data-char={char}
                    aria-description={`input for ${char}`}
                    ref={inputRef}
                    size={1}
                    minLength={1}
                    maxLength={1}
                    onChange={handleInput}
                    onFocus={setHighlightedInput}
                    value={inputState.get(char) || ""}
                />
                </div>
            </StyledContainer>
    )
}


const Whitespace = styled.div`
    width: 64px;
    height: 64px;
`

const Special = styled.div<{ $content: string }>`
    width: 64px;
    height: 64px;
    &:before {
        content: '${props => props.$content}';
    }
`

const Character = { Input, Whitespace, Special  }
Character.Input = Input
Character.Whitespace = Whitespace
Character.Special = Special
export default Character
