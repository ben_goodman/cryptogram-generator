import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import Cryptogram from 'src/components/Cryptogram'
import { type CryptogramRequest } from 'src/useCryptogram'
import { deserializeState } from 'src/util/serde'


const mockData = {
    "encoded": "loading",
    "plaintextHash": 101234,
    "author": "loading"
}

export const Component = () => {
    const [
        {data, loading, error},
        setCryptogram
    ] = useState<CryptogramRequest>({data: mockData, loading: true, error: false})

    const { state } = useParams()

    useEffect(() => {
        if (state) {
            const data = JSON.parse(deserializeState(state))
            setCryptogram(data)
        }
    }, [])


    return <Cryptogram {...data!} />

}

Component.displayName = 'ResumeState'
