import React, { useEffect } from 'react'

import Cryptogram from 'src/components/Cryptogram'
import useCryptogram from 'src/useCryptogram'
import { serializeState } from 'src/util/serde'


const mockData = {
    "encoded": "loading",
    "plaintextHash": 101234,
    "author": "loading"
}

export const Component = () => {
    const cryptogram = useCryptogram()
    const { loading, error, data } = cryptogram

    // serialize state on load
    useEffect(() => {
        if (data) {
            const serializedState = serializeState(JSON.stringify(cryptogram))
            // store as cookie
            document.cookie = `cryptogram=${serializedState};max-age=31536000;path=/`
        }
    }, [cryptogram.data])

    return (
        <>
            {loading && <Cryptogram {...mockData} />}
            {error && <Cryptogram {...mockData} />}
            {data && <Cryptogram {...data} />}
        </>
    )

}

Component.displayName = 'Random'
