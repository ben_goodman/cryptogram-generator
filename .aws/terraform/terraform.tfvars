project_name = "cryptograms-app"
resource_namespace = "bgoodman"
hosted_zone_name = "ben.website"
website_aliases  = ["cryptograms.ben.website", "www.cryptograms.ben.website"]
acm_certificate_arn = "arn:aws:acm:us-east-1:757687723154:certificate/f4348a56-1a12-4a0e-9068-973c3b22bb93"
