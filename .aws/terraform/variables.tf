variable "project_name" {
    type = string
    description = "The name of the project."
}

variable "resource_namespace" {
    type = string
    description = "A namespace to use for all resources."
}

variable "hosted_zone_name" {
    type = string
    description = "The name of the Route53 hosted zone to create the DNS record in."
}

variable "website_aliases" {
    type = list(string)
    description = "A list of alternate domains for the website."
    default = []
}

variable "acm_certificate_arn" {
    type = string
    description = "The ARN of the ACM certificate to use for HTTPS."
}
