import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { generateCryptogram } from './generateCryptogram'

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const cryptogram = await generateCryptogram()
    return {
        statusCode: 200,
        body: JSON.stringify(cryptogram)
    }
}
