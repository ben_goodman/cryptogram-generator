import { shuffleArray } from './util'
import { getQuote } from './getQuote'

export interface Cryptogram {
    encoded: string
    plaintextHash: number
    author: string
}

export const encodeString = (string: string): string => {

    const alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    const shuffledAlphabet = shuffleArray(['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'])

    const cipher = new Map<string, string>()

    alphabet.forEach((letter, index) => cipher.set(letter, shuffledAlphabet[index]))

    const encodedString = string.split('').map(letter => cipher.get(letter) || letter)

    return encodedString.join('')
}

export const hashString = (str: string) => {
    let hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
      chr = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
}


export const generateCryptogram = async (): Promise<Cryptogram> => {
    const {q, a} = await getQuote()
    const encoded = encodeString(q)
    console.log('debug: ', q)
    return {
        encoded,
        plaintextHash: hashString(q),
        author: a
    }
}