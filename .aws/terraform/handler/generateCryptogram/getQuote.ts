import { normalizeQuote } from './util';
type QuoteText = string
type QuoteAuthor = string

export interface Quote {
    q: QuoteText
    a: QuoteAuthor
}

const handleAPIResponse = (resp: any): Promise<Quote> => (
    resp.json().then( data => {
        if (data.length !== 1) {
            console.error(JSON.stringify(data))
            throw new Error('Unexpected response from API.')
        } else {
            return normalizeQuote(data[0])
        }
    })
)

const handleAPIError = (err: any): Promise<Quote> => {
    console.error('Failure fetching API.')
    console.error(err)
    return Promise.reject(err)
}

export const getQuote = async (): Promise<Quote> => (
    fetch(process.env.QUOTES_GENERATOR_ENDPOINT_URL!)
        .catch(handleAPIError)
        .then(handleAPIResponse)
)


