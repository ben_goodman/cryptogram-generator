import { type Quote } from './getQuote'

export const normalizeQuote = ({q, ...p}: Quote): Quote => {
    const normalString = q.normalize("NFD")
        .replace(/\p{Diacritic}/gu, "")
        .toLowerCase()
        .trim()
        .replace(/\s{2,}/g, ' ')
    return {q: normalString, ...p}

}

export const shuffleArray = <T>(array: Array<T>): Array<T> => {
    let currentIndex = array.length,  randomIndex;
    const _array = array

    // While there remain elements to shuffle.
    while (currentIndex > 0) {

      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [_array[currentIndex], _array[randomIndex]] = [
        _array[randomIndex], _array[currentIndex]];
    }

    return _array;
}