import { test, expect,  } from '@jest/globals'
import { normalizeQuote } from "../util";

test('normalizeQuote', () => {
    const quote = {
        q: "The quick brown fox jumps over the lazy dog.",
        a: "Anonymous"
    }
    const normalizedQuote = normalizeQuote(quote)
    expect(normalizedQuote.q).toEqual("the quick brown fox jumps over the lazy dog.")
})

