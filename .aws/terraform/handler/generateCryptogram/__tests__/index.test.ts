import { test, expect,  } from '@jest/globals'
import { encodeString, hashString } from '../index'

const mockMath = Object.create(global.Math);
mockMath.random = () => 0.5;
global.Math = mockMath;

test('encodeString', () => {
    const cryptogram = encodeString("abcdefghijklmnopqrstuvwxyz")
    expect(cryptogram).toHaveLength(26)
    expect(cryptogram).toEqual("arbzcvdpetfxgohqisjukwlymn")
    expect( new Set(cryptogram.split('')).size ).toBe(26)
})

test('hashString', () => {
    const hash = hashString("abcdefghijklmnopqrstuvwxyz")
    expect(hash).toEqual(958031277)
})

