resource "random_id" "cd_function_suffix" {
    byte_length = 2
}

data "archive_file" "lambda_payload_cryptogram_generator" {
    type             = "zip"
    source_file      = "${path.module}/dist/index.js"
    output_file_mode = "0666"
    output_path      = "${path.module}/dist/index.js.zip"
}

module "cryptogram_generator" {
    source = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.3.0"
    org              = "bgoodman"
    project_name     = "cgram-gen"
    lambda_payload   = data.archive_file.lambda_payload_cryptogram_generator
    function_name    = "cgram-gen-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    timeout          = 3
    memory_size      = 256
}

resource "aws_lambda_function_url" "cryptogram_generator_endpoint" {
  function_name      = module.cryptogram_generator.name
  authorization_type = "NONE"
}

resource "aws_cloudfront_cache_policy" "default" {
    name        = "default-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 3600
    max_ttl     = 86400
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

resource "aws_cloudfront_cache_policy" "random_cryptogram_generator" {
    name        = "random-cryptogram-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 5
    max_ttl     = 5
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "whitelist"
            cookies {
                items = [ "cryptogram" ]
            }
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

module "website" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.0.0"

    org          = var.resource_namespace
    project_name = var.project_name
    aliases      = var.website_aliases
    use_cloudfront_default_certificate = false
    acm_certificate_arn = var.acm_certificate_arn
    default_cache_policy_id = aws_cloudfront_cache_policy.default.id

    additional_custom_origins = {
        "cryptogram-generator" = {
            domain_name = trimsuffix(trimprefix(aws_lambda_function_url.cryptogram_generator_endpoint.function_url, "https://"), "/")
        }
    }

    ordered_cache_behaviors = {
        "cryptogram-generator" = {
            path_pattern = "/cryptogram/*"
            viewer_protocol_policy = "https-only"
            cache_policy_id = aws_cloudfront_cache_policy.random_cryptogram_generator.id
        }
    }

}

module "dns_alias" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/dns"
    version = "2.0.0"

    hosted_zone_name       = var.hosted_zone_name
    cloudfront_domain_name = module.website.cloudfront_default_domain
    domain_name            = var.website_aliases
}
