


```mermaid

sequenceDiagram
    autonumber

    /cryptogram/*->>+CloudFront: Client wants a Cryptogram
    activate CloudFront

    alt Cookie Exists

        CloudFront->>+/cryptogram/*: Finish existing one first
        deactivate CloudFront

    else

        CloudFront->>API: Client does not have a cryptogram
        activate API

        API->>+ZenQuotes: I need a quotation
        ZenQuotes->>-API: Apply substitution

        API->>+CloudFront: Here's a cryptogram
        deactivate API

        CloudFront->>+/cryptogram/*: Client accepts cryptogram
        deactivate CloudFront

    end
```